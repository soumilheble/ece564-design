
source setup.tcl
set begintime [clock seconds]
open_mw_lib ./work
open_mw_cel ${modname}_placed


check_legality

set_clock_tree_options -clock_trees $clkname \
		-insert_boundary_cell true \
		-ocv_clustering true \
		-buffer_relocation true \
		-buffer_sizing true \
		-gate_relocation true \
		-gate_sizing true
		
set cts_use_debug_mode true
set cts_do_characterization true


clock_opt -fix_hold_all_clocks  -continue_on_missing_scandef

# DEFINING POWER/GROUND NETS AND PINS			 
#derive_pg_connection -power_net VDD		\
#			 -ground_net VSS	\
#			 -power_pin VDD		\
#			 -ground_pin VSS	
			 
preroute_standard_cells -nets VSS -connect horizontal
preroute_standard_cells -nets VDD -connect horizontal

verify_pg_nets
verify_pg_nets  -pad_pin_connection all

report_timing

save_mw_cel -as ${modname}_cts
set endtime [clock seconds]
set timestr [timef [expr $endtime-$begintime]]
puts "run_cts.tcl completed successfully (elapsed time: $timestr actual)"
exit
