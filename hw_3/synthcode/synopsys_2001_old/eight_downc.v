/*
 * Name: eight_downc.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * RTL Code for Eight Bit Down Counter
 */
 
module eight_downc (input clock, 
					input latch, 
					input dec, 
					input div_by_two,
					input [7:0] in, 
					output wire zero);

	reg [7:0] count_value;

	always@(posedge clock)
		begin
			if(latch)
				count_value <= in;
			else if(dec && !zero)
				count_value <= count_value - 1'b1;
			else if(div_by_two && !dec && !zero)
				count_value <= count_value >> 1;
		end
		
	assign zero = ~|count_value;
endmodule
