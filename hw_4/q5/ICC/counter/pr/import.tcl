source  setup.tcl



create_mw_lib -technology "${TECH_ROOT}/ece720/Nangate_45nm_OCL_Milkyway_v2009_07/FreePDK45.tf" \
    -mw_reference_library \
    "${TECH_ROOT}/ece720/Nangate_45nm_OCL_Milkyway_v2009_07/Nangate_45nm_OCL_v2009_07" \
    -hier_separator {/} \
    -bus_naming_style {[%d]} \
    -open ./work

import_designs -format verilog \
    -top ${modname} \
    -cel ${modname}_init ${GATE_DIR}/${modname}_final.v

source  constraints.tcl
