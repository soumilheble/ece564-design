/*
 * Name: eight_downc_95.v
 * Date Created: 09/16/2018 19:12
 * Author: Soumil Krishnanand Heble
 * Module Name: eight_downc_95
 * RTL Code for Eight Bit Down Counter in Verilog 95 Format
 */

/* Module Declaration */ 
module eight_downc_95 (clock, in, latch, dec, div_by_two, zero);
	
	/* Inputs */
	input clock;
	input [7:0] in;
	input latch;
	input dec;
	input div_by_two;
	
	/* Outputs */
	output zero;
	
	/* Variable (Nets and Regs) Declaration */
	reg [7:0] count_value;
	wire zero;
	
	/* RTL Code */
	always@(posedge clock)
		begin
			if(latch)	// Take value from in to count_value reg when latch is set
				count_value <= in;
			else if(dec && !zero)	// Decrement count_value when dec is set and zero is not asserted
				count_value <= count_value - 1'b1;
			else if(div_by_two && !zero)	// Divide count_value reg by two when div_by_two is high and zero is cleared
				count_value <= (count_value>>1);
		end
	/* Combinational Logic for Zero Flag */
	assign zero = ~|count_value;
endmodule
