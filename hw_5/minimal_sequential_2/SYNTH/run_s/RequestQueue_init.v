
module RequestQueue ( clock, reset, R0, R1, G0, G1 );
  input clock, reset, R0, R1;
  output G0, G1;
  wire   \U5/Z_0 , \U5/Z_1 , \U4/Z_0 , n41, n42, n43, n44, n45, n46, n48, n49;
  assign G0 = \U4/Z_0 ;

  DFFR_X1 \current_state_reg[0]  ( .D(\U5/Z_0 ), .CK(clock), .RN(reset), .Q(
        n48), .QN(n42) );
  DFFR_X1 \current_state_reg[1]  ( .D(\U5/Z_1 ), .CK(clock), .RN(reset), .Q(
        n49), .QN(n41) );
  XOR2_X2 U6 ( .A(n43), .B(R0), .Z(n44) );
  NAND2_X2 U9 ( .A1(n49), .A2(n42), .ZN(n46) );
  NAND2_X2 U10 ( .A1(n48), .A2(n41), .ZN(n45) );
  INV_X4 U11 ( .A(R1), .ZN(n43) );
  AND2_X4 U12 ( .A1(n48), .A2(n49), .ZN(G1) );
  NAND2_X2 U13 ( .A1(n44), .A2(n45), .ZN(\U5/Z_1 ) );
  NAND2_X2 U14 ( .A1(n43), .A2(n45), .ZN(\U5/Z_0 ) );
  NAND2_X2 U15 ( .A1(n45), .A2(n46), .ZN(\U4/Z_0 ) );
endmodule

