/*
 * Name: StatCount_test.v
 * Date/Time: 10/20/2018 02:56
 * Author: Soumil Krishnanand Heble
 * ECE564 HW6 Stream Statistic Counter Test Fixture
 */

module testfixture;

reg clock, reset, clear; 
reg [7:0] DataIn1, DataIn2;
wire [7:0] EvenParity, GreyCode;
wire overflow;

initial
	begin
		    reset = 1;
		    DataIn1 = 8'h0;
		    DataIn2 = 8'h0;
		    clock = 0;
		    clear = 1;
		
		#10 clear = 0;
		    DataIn1 = 8'h2; 
	    	    DataIn2 = 8'h3;

		#10 DataIn1 = 8'hAA; 
	    	    DataIn2 = 8'h3;

		#10 DataIn1 = 8'hAA; 
	    	    DataIn2 = 8'h55;	
		
		#1300 $finish;
		end

always #5 clock = ~clock;

StatCount U0 (.clock(clock), .reset(reset), .clear(clear), .DataIn1(DataIn1), .DataIn2(DataIn2), .EvenParity(EvenParity), .GreyCode(GreyCode), .overflow(overflow));

endmodule
