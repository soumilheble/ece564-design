/*
 * Name: RequestQueue_test.v
 * Date/Time: 10/12/2018 23:44
 * Author: Soumil Krishnanand Heble
 * ECE564 HW5 Arbiter FSM Test Fixture
 */

module testfixture;

reg clock, reset, R0, R1;
wire G0, G1;

initial
	begin
		    reset = 1;
		    R0 = 0;
		    R1 = 0;
		    clock = 0;
		#10 reset = 0;
		#10 reset = 1;
		
		/* Test 1 */	
		#10 R0 = 1; 
	    	    R1 = 1;
		#10 if (G0 && !G1) 
			$display("T1 OK\n"); 
		    else 
			$display("T1 Error\n");
		
		/* Test 2 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (!G0 && G1) 
			$display("T2 OK\n"); 
		    else 
			$display("T2 Error\n");
		
		/* Test 3 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (G0 && !G1) 
			$display("T3 OK\n"); 
		    else 
			$display("T3 Error\n");
		
		/* Test 4 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (G0 && !G1) 
			$display("T4 OK\n"); 
		    else 
			$display("T4 Error\n");
		
		/* Test 5 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (G0 && !G1) 
			$display("T5 OK\n"); 
		    else 
			$display("T5 Error\n");
		
		/* Test 6 */	
		    R0 = 1; 
	    	    R1 = 1;
		#10 if (G0 && !G1) 
			$display("T6 OK\n"); 
		    else 
			$display("T6 Error\n");
		
		/* Test 7 */	
		    R0 = 0; 
	    	    R1 = 1;
		#10 if (!G0 && G1) 
			$display("T7 OK\n"); 
		    else 
			$display("T7 Error\n");
		
		/* Test 8 */	
		    R0 = 0; 
	    	    R1 = 1;
		#10 if (!G0 && G1) 
			$display("T8 OK\n"); 
		    else 
			$display("T8 Error\n");
		
		/* Test 9 */	
		    R0 = 1; 
	    	    R1 = 1;
		#10 if (G0 && !G1) 
			$display("T9 OK\n"); 
		    else 
			$display("T9 Error\n");
		
		/* Test 10 */	
		    R0 = 1; 
	    	    R1 = 1;
		#10 if (!G0 && G1) 
			$display("T10 OK\n"); 
		    else 
			$display("T10 Error\n");
		
		/* Test 11 */	
		    R0 = 1; 
	    	    R1 = 1;
		#10 if (G0 && !G1) 
			$display("T11 OK\n"); 
		    else 
			$display("T11 Error\n");
		
		/* Test 12 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (!G0 && G1) 
			$display("T12 OK\n"); 
		    else 
			$display("T12 Error\n");
		
		/* Test 13 */	
		    R0 = 1; 
	    	    R1 = 0;
		#10 if (G0 && !G1) 
			$display("T13 OK\n"); 
		    else 
			$display("T13 Error\n");
		
		/* Test 14 */	
		    R0 = 0; 
	    	    R1 = 1;
		#10 if (!G0 && G1) 
			$display("T14 OK\n"); 
		    else 
			$display("T14 Error\n");
		
		/* Test 15 */	
		    R0 = 0; 
	    	    R1 = 0;
		#10 if (!G0 && !G1) 
			$display("T15 OK\n"); 
		    else 
			$display("T15 Error\n");
		
		/* Test 16 */	
		    R0 = 0; 
	    	    R1 = 0;
		#10 if (!G0 && !G1) 
			$display("T16 OK\n"); 
		    else 
			$display("T16 Error\n");
		
		#100 $finish;
		end

always #5 clock = ~clock;

RequestQueue U0 (.clock(clock), .reset(reset), .R0(R0), .R1(R1), .G0(G0), .G1(G1));

endmodule
