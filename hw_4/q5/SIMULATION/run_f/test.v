/*
 * Name: hw3_test_95.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * Module Name: hw3_test_95
 * Test Script for Eight Bit Down Counter
 */ 

module test_fixture;
	
	/* Test Bench Variables */
	reg clock100 = 0;
	reg latch = 0;
	reg dec = 0;
	reg div_by_two = 0;
	reg [7:0] in = 8'b10000000;	//Load 0x80 (128) in the in reg
	wire zero;
	
	initial
		begin
			$dumpfile("count.vcd");	//Dump waveform to a file
			$dumpvars;	//Dump all waveforms to the file
			#16 latch = 1;	//wait 16ns then set latch (take in value from in to internal count_value reg) 
			#10 latch = 0;	//wait 10ns then clear latch 
			#10 div_by_two = 1;	//wait 10ns then start divind count_value 
			#30 div_by_two = 0;	//After 30ns count_value becomes 0x10 (16)
			#10 dec = 1;		//wait 10ns then start decrementing count_value
			#80 dec = 0;	//wait 80ns the count_value drops to 0x08 (8)
			#10 div_by_two = 1;	//set div_by_two
			#20 div_by_two = 0;	//wait 20ns clear div_by_two the count_value drops to 0x02 (2)
			#10 dec = 1;	//set dec
			#30 $finish;	//wait 30ns then finish zero is set and count_value drops to 0x00 (0)
		end
	always #5 clock100 = ~clock100;	//Toggle clock every 5ns (10ns period with 50% duty)
	
	/* Instantiate Down Counter Module with name edc_95) */
	counter u1 (.clock(clock100), .in(in), .latch(latch), .dec(dec), .div_by_two(div_by_two), .zero(zero));
endmodule
