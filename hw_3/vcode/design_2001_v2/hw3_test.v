/*
 * Name: hw3_test.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * Module Name: hw3_test
 * Test Script for Eight Bit Down Counter
 */
 
module hw3_test;
	
	/* Variable Declaration */
	reg clock100 = 0;
	reg latch = 0;
	reg dec = 0;
	reg div_by_two = 0;
	reg [7:0] in = 8'b00001100;	// Load 0x0C (12) to the in reg
	wire zero;
	
	initial
		begin
			$dumpfile("hw3_test.vcd");	// Dump waveforms to this file
			$dumpvars;	// Dump all waveforms to file
			#16 latch = 1;	// Wait 16ns set latch to bring in data to internal count_value reg 
			#10 latch = 0;	// Wait 10ns clear latch 
			#10 dec = 1;	// Wait 10ns set dec to begin decrementing count_value reg 
			#80 dec = 0;	// Wait 80ns count_value reg falls down to 0x04 (4)
			div_by_two = 1;	// Set div_by_two to begin dividing count_value reg by two
			#20 div_by_two = 0;	// Wait 20ns clear div_by_two count_value reg drops to 0x01 (1)
			dec = 1;	// Set dec
			#10 $finish;	// Wait 10ns count_value drops to 0x00 (0) and zero is asserted
		end
	always #5 clock100 = ~clock100;	// Toggle clock every 5ns (Period 10ns - 50% duty)
	
	/* Instantiate Down Counter Module with edc */
	eight_downc edc (.clock(clock100), .latch(latch), .dec(dec), .div_by_two(div_by_two), .in(in), .zero(zero));
endmodule
