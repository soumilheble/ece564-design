/*
 * Name: hw3_test.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * Test Script for Eight Bit Down Counter
 */
 
module hw3_test;
	reg clock100 = 0;
	reg latch = 0;
	reg dec = 0;
	reg div_by_two = 0;
	reg [7:0] in = 8'b00001100;
	wire zero;
	
	initial
		begin
			$dumpfile("hw3_test.vcd");
			$dumpvars;
			#16 latch = 1;
			#10 latch = 0;
			#10 dec = 1;
			#80 dec = 0;
			div_by_two = 1;
			#20 div_by_two = 0;
			dec = 1;
			#10 $finish;
		end
	always #5 clock100 = ~clock100;
	
	eight_downc edc (.clock(clock100), .latch(latch), .dec(dec), .div_by_two(div_by_two), .in(in), .zero(zero));
endmodule
