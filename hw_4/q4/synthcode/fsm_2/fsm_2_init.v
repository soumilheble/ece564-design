
module fsm_2 ( clock, reset, start, done_flag, match_address, inc_flag, 
        location, outcell );
  input [8:0] match_address;
  output [8:0] location;
  output [8:0] outcell;
  input clock, reset, start, done_flag;
  output inc_flag;
  wire   n1, n2, n3, n4, n5, n6, n7, n8, \U5/Z_0 , \U5/DATA1_0 , \U4/Z_0 ,
         \U4/Z_1 , \U4/Z_2 , \U4/Z_3 , \U4/Z_4 , \U4/Z_5 , \U4/Z_6 , \U4/Z_7 ,
         \U4/Z_8 , \U4/DATA2_0 , \U4/DATA2_1 , \U4/DATA2_2 , \U4/DATA2_3 ,
         \U4/DATA2_4 , \U4/DATA2_5 , \U4/DATA2_6 , \U4/DATA2_7 , \U4/DATA2_8 ,
         n52, n53, n54, n55, n56;
  assign \U5/DATA1_0  = start;
  assign location[0] = \U4/Z_0 ;
  assign location[1] = \U4/Z_1 ;
  assign location[2] = \U4/Z_2 ;
  assign location[3] = \U4/Z_3 ;
  assign location[4] = \U4/Z_4 ;
  assign location[5] = \U4/Z_5 ;
  assign location[6] = \U4/Z_6 ;
  assign location[7] = \U4/Z_7 ;
  assign location[8] = \U4/Z_8 ;
  assign \U4/DATA2_0  = match_address[0];
  assign \U4/DATA2_1  = match_address[1];
  assign \U4/DATA2_2  = match_address[2];
  assign \U4/DATA2_3  = match_address[3];
  assign \U4/DATA2_4  = match_address[4];
  assign \U4/DATA2_5  = match_address[5];
  assign \U4/DATA2_6  = match_address[6];
  assign \U4/DATA2_7  = match_address[7];
  assign \U4/DATA2_8  = match_address[8];

  NOR2_X2 U3 ( .A1(n53), .A2(n56), .ZN(n8) );
  XNOR2_X2 U4 ( .A(\U4/DATA2_7 ), .B(\U4/DATA2_8 ), .ZN(n53) );
  XNOR2_X2 U5 ( .A(\U4/Z_6 ), .B(n54), .ZN(n7) );
  XNOR2_X2 U6 ( .A(n55), .B(\U4/Z_5 ), .ZN(n6) );
  OAI22_X2 U12 ( .A1(done_flag), .A2(n56), .B1(inc_flag), .B2(n52), .ZN(
        \U5/Z_0 ) );
  INV_X4 U13 ( .A(n54), .ZN(\U4/Z_7 ) );
  NAND2_X2 U14 ( .A1(inc_flag), .A2(\U4/DATA2_7 ), .ZN(n54) );
  INV_X4 U15 ( .A(n55), .ZN(\U4/Z_6 ) );
  NAND2_X2 U16 ( .A1(\U4/DATA2_6 ), .A2(inc_flag), .ZN(n55) );
  INV_X4 U18 ( .A(\U5/DATA1_0 ), .ZN(n52) );
  DFF_X2 \outcell_reg[0]  ( .D(n1), .CK(clock), .Q(outcell[0]) );
  DFF_X2 \outcell_reg[1]  ( .D(n2), .CK(clock), .Q(outcell[1]) );
  DFF_X2 \outcell_reg[2]  ( .D(n3), .CK(clock), .Q(outcell[2]) );
  DFF_X2 \outcell_reg[3]  ( .D(n4), .CK(clock), .Q(outcell[3]) );
  DFF_X2 \outcell_reg[4]  ( .D(n5), .CK(clock), .Q(outcell[4]) );
  DFF_X2 \outcell_reg[6]  ( .D(n7), .CK(clock), .Q(outcell[6]) );
  DFF_X2 \outcell_reg[7]  ( .D(n8), .CK(clock), .Q(outcell[7]) );
  DFF_X2 \outcell_reg[8]  ( .D(\U4/Z_8 ), .CK(clock), .Q(outcell[8]) );
  DFF_X2 \outcell_reg[5]  ( .D(n6), .CK(clock), .Q(outcell[5]) );
  DFFR_X2 current_state_reg ( .D(\U5/Z_0 ), .CK(clock), .RN(reset), .Q(
        inc_flag), .QN(n56) );
  AND2_X4 U26 ( .A1(\U4/DATA2_0 ), .A2(inc_flag), .ZN(\U4/Z_0 ) );
  AND2_X4 U27 ( .A1(\U4/DATA2_4 ), .A2(inc_flag), .ZN(\U4/Z_4 ) );
  AND2_X4 U28 ( .A1(\U4/DATA2_3 ), .A2(inc_flag), .ZN(\U4/Z_3 ) );
  AND2_X4 U29 ( .A1(\U4/DATA2_2 ), .A2(inc_flag), .ZN(\U4/Z_2 ) );
  AND2_X4 U30 ( .A1(\U4/DATA2_1 ), .A2(inc_flag), .ZN(\U4/Z_1 ) );
  AND2_X4 U31 ( .A1(\U4/DATA2_8 ), .A2(inc_flag), .ZN(\U4/Z_8 ) );
  XOR2_X1 U32 ( .A(\U4/Z_4 ), .B(\U4/Z_3 ), .Z(n4) );
  XOR2_X1 U33 ( .A(\U4/Z_3 ), .B(\U4/Z_2 ), .Z(n3) );
  XOR2_X1 U34 ( .A(\U4/Z_2 ), .B(\U4/Z_1 ), .Z(n2) );
  XOR2_X1 U35 ( .A(\U4/Z_1 ), .B(\U4/Z_0 ), .Z(n1) );
  XOR2_X1 U36 ( .A(\U4/Z_5 ), .B(\U4/Z_4 ), .Z(n5) );
  AND2_X4 U37 ( .A1(\U4/DATA2_5 ), .A2(inc_flag), .ZN(\U4/Z_5 ) );
endmodule

