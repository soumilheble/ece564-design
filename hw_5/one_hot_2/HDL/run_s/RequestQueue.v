/*
 * Name: RequestQueue.v
 * Date/Time: 10/12/2018 22:07
 * Author: Soumil Krishnanand Heble
 * ECE564 HW5 Arbiter Verilog Code - Minimal Sequential Encoding
 */

/* Module and Port Declaration - Verilog 2001 Format */
module RequestQueue (input clock, input reset, input wire R0, input wire R1, output reg G0, output reg G1);

/* State Machine - State Encoding */
/* Sn = State n, Refer FSD for the FSM Flow and Detailed State Information */
parameter [3:0] // synopsys enum states
	S0 = 4'b0001,
	S1 = 4'b0010,
	S2 = 4'b0100,
	S3 = 4'b1000;

/* current_state -> State Register and next_state signal */
reg [3:0] /* synopsys enum states */ current_state, next_state;
// synopsys state_vector current_state

/*------- Sequential Logic ----*/
always@(posedge clock, negedge reset)
begin
	if(!reset)
	begin
		current_state <= S0;
	end
	else
	begin
		current_state <= next_state;
	end
end

/* Next State Logic and Output Logic using Case Statement */
always@(*)
begin
	/* defaults to prevent latches */
	G0 = 0;
	G1 = 0;
	/* FSM implementation using case */
	case(current_state) // synopsys full_case parallel_case
		S0: begin
			G0 = 0; 
			G1 = 0;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S2;
			end
			else
			begin
				next_state = S3;
			end
		    end
		S1: begin
			G0 = 1; 
			G1 = 0;
			next_state = S3;
		    end
		S2: begin
			G0 = 1;
			G1 = 0;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S2;
			end
			else
			begin
				next_state = S3;
			end
		    end
		S3: begin
			G0 = 0;
			G1 = 1;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S2;
			end
			else
			begin
				next_state = S3;
			end
		    end
		default: next_state = S0;
	endcase
end
endmodule
