/*
 * Name: eight_downc.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * Module Name: eight_downc
 * RTL Code for Eight Bit Down Counter in Verilog 2001 Format
 */ 

/* Module Declarations */ 
module eight_downc (input clock,
					input latch,
					input dec,
					input div_by_two,
					input [7:0] in,
					output wire zero);
	
	/* Variable (Nets and Regs) Declaration */
	reg [7:0] count_value;
	
	/* RTL Code */
	always@(posedge clock)
		begin
			if(latch)	// Bring in value from in to count_value reg when latch is set
				count_value <= in;
			else if(dec && !zero)	// Start decrementing count_value reg when dec is set and zero is not asserted
				count_value <= count_value - 1'b1;
			else if(div_by_two && !zero)	//Divide count_value reg by two when div_by_two is set and dec, zero are clear
				count_value <= count_value >> 1;
		end
	
	/* Combinational Logic for Zero Flag */
	assign zero = ~|count_value;
endmodule
