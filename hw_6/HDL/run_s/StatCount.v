/*
 * Name: StatCount.v
 * Date/Time: 10/20/2018 01:58
 * Author: Soumil Krishnanand Heble
 * ECE564 HW6 Stream Statistic Counter
 */

/* Module and Port Declaration - Verilog 2001 Format */
module StatCount (input clock, input reset, input clear, input [7:0] DataIn1, input [7:0] DataIn2, output reg [7:0] EvenParity, output reg [7:0] GreyCode, output reg overflow);

reg d1_parity_result;
reg d2_parity_result;
reg [1:0] parity_inc_muxout;
reg [7:0] EvenParity_count;
reg EvenParity_count_carry;

reg d1_grey_result;
reg d2_grey_result;
reg [1:0] grey_inc_muxout;
reg [7:0] GreyCode_count;
reg GreyCode_count_carry;

reg overflow_val;

always@(posedge clock, negedge reset)
begin
	if(!reset)
	begin
		EvenParity <= 8'b0;
		GreyCode <= 8'b0;
		overflow <= 1'b0;
	end
	else
	begin
		EvenParity <= EvenParity_count;
		GreyCode <= GreyCode_count;
		overflow <= overflow_val;
	end
end

always@(*)
begin
	d1_parity_result = ~(^DataIn1);
	d2_parity_result = ~(^DataIn2);
	casex({d1_parity_result, d2_parity_result})
		2'b00: begin
			parity_inc_muxout = 2'b00;
		end

		2'b11: begin
			parity_inc_muxout = 2'b10;
		end

		2'bxx: begin
			parity_inc_muxout = 2'b01;
		end
	endcase 

	if(clear==0)
	begin
		{EvenParity_count_carry,EvenParity_count} = EvenParity + parity_inc_muxout;
	end
	else
	begin
		EvenParity_count_carry = 1'b0;
		EvenParity_count = 8'b00000000;
	end

	/*
	if((DataIn1==8'hAA)||(DataIn2==8'h55))
	begin
		d1_grey_result = 1'b1;
	end
	else
	begin
		d1_grey_result = 1'b0;
	end

	if((DataIn2==8'hAA)||(DataIn2==8'h55))
	begin
		d2_grey_result = 1'b1;
	end
	else
	begin
		d2_grey_result = 1'b0;
	end
	*/

	d1_grey_result = &{(DataIn1[7]^DataIn1[6]),(DataIn1[5]^DataIn1[4]),(DataIn1[3]^DataIn1[2]),(DataIn1[1]^DataIn1[0])};
	d2_grey_result = &{(DataIn2[7]^DataIn2[6]),(DataIn2[5]^DataIn2[4]),(DataIn2[3]^DataIn2[2]),(DataIn2[1]^DataIn2[0])};

	casex({d1_grey_result, d2_grey_result})
		2'b00: begin
			grey_inc_muxout = 2'b00;
		end

		2'b11: begin
			grey_inc_muxout = 2'b10;
		end

		2'bxx: begin
			grey_inc_muxout = 2'b01;
		end
	endcase

	if(clear==0)
	begin
		{GreyCode_count_carry, GreyCode_count} = GreyCode + grey_inc_muxout;
	end
	else
	begin
		GreyCode_count_carry = 1'b0;
		GreyCode_count = 8'b00000000;
	end

	if(clear==0)
	begin
		if(overflow==1'b1)
		begin
			overflow_val = 1'b1;
		end
		else
		begin
			overflow_val = EvenParity_count_carry|GreyCode_count_carry;
		end
	end
	else
	begin
		overflow_val = 1'b0;
	end
end
endmodule
