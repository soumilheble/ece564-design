
module eight_downc ( clock, latch, dec, div_by_two, in, zero );
  input [7:0] in;
  input clock, latch, dec, div_by_two;
  output zero;
  wire   U4_DATA3_0, U4_DATA3_1, U4_DATA3_2, U4_DATA3_3, U4_DATA3_4,
         U4_DATA3_5, U4_DATA3_6, U4_DATA2_1, U4_DATA2_2, U4_DATA2_3,
         U4_DATA2_4, U4_DATA2_5, U4_DATA2_6, U4_DATA2_7, sub_26_carry_2_,
         sub_26_carry_3_, sub_26_carry_4_, sub_26_carry_5_, sub_26_carry_6_,
         sub_26_carry_7_, sub_26_A_0_, n54, n55, n56, n57, n58, n59, n60, n61,
         n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
         n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89,
         n90, n91, n92, n93, n94;

  XNOR2_X2 sub_26_U1_A_2 ( .A(U4_DATA3_1), .B(sub_26_carry_2_), .ZN(U4_DATA2_2) );
  OR2_X1 sub_26_U1_B_2 ( .A1(U4_DATA3_1), .A2(sub_26_carry_2_), .ZN(
        sub_26_carry_3_) );
  XNOR2_X2 sub_26_U1_A_3 ( .A(U4_DATA3_2), .B(sub_26_carry_3_), .ZN(U4_DATA2_3) );
  XNOR2_X2 sub_26_U1_A_4 ( .A(U4_DATA3_3), .B(sub_26_carry_4_), .ZN(U4_DATA2_4) );
  XNOR2_X2 sub_26_U1_A_5 ( .A(U4_DATA3_4), .B(sub_26_carry_5_), .ZN(U4_DATA2_5) );
  OR2_X1 sub_26_U1_B_5 ( .A1(U4_DATA3_4), .A2(sub_26_carry_5_), .ZN(
        sub_26_carry_6_) );
  XNOR2_X2 sub_26_U1_A_6 ( .A(U4_DATA3_5), .B(sub_26_carry_6_), .ZN(U4_DATA2_6) );
  XNOR2_X2 sub_26_U1_A_7 ( .A(U4_DATA3_6), .B(sub_26_carry_7_), .ZN(U4_DATA2_7) );
  DFF_X1 count_value_reg_6_ ( .D(n85), .CK(clock), .Q(U4_DATA3_5), .QN(n57) );
  DFF_X1 count_value_reg_5_ ( .D(n86), .CK(clock), .Q(U4_DATA3_4), .QN(n58) );
  DFF_X1 count_value_reg_4_ ( .D(n87), .CK(clock), .Q(U4_DATA3_3), .QN(n59) );
  DFF_X1 count_value_reg_3_ ( .D(n88), .CK(clock), .Q(U4_DATA3_2), .QN(n60) );
  DFF_X1 count_value_reg_1_ ( .D(n90), .CK(clock), .Q(U4_DATA3_0), .QN(n62) );
  OAI221_X2 U5 ( .B1(n64), .B2(n67), .C1(n56), .C2(n58), .A(n74), .ZN(n86) );
  OAI221_X2 U7 ( .B1(n64), .B2(n68), .C1(n56), .C2(n59), .A(n75), .ZN(n87) );
  OAI221_X2 U9 ( .B1(n64), .B2(n69), .C1(n56), .C2(n60), .A(n76), .ZN(n88) );
  OAI221_X2 U11 ( .B1(n64), .B2(n70), .C1(n56), .C2(n61), .A(n77), .ZN(n89) );
  OAI221_X2 U13 ( .B1(n64), .B2(n71), .C1(n56), .C2(n62), .A(n78), .ZN(n90) );
  NOR3_X2 U19 ( .A1(n73), .A2(latch), .A3(n63), .ZN(n80) );
  NOR4_X2 U22 ( .A1(U4_DATA3_3), .A2(U4_DATA3_2), .A3(U4_DATA3_1), .A4(
        U4_DATA3_0), .ZN(n83) );
  NAND3_X2 U23 ( .A1(n65), .A2(n64), .A3(div_by_two), .ZN(n81) );
  INV_X4 U24 ( .A(n73), .ZN(n55) );
  INV_X4 U25 ( .A(n80), .ZN(n56) );
  INV_X4 U26 ( .A(n81), .ZN(n63) );
  INV_X4 U27 ( .A(latch), .ZN(n64) );
  INV_X4 U28 ( .A(dec), .ZN(n65) );
  INV_X4 U29 ( .A(in[6]), .ZN(n66) );
  INV_X4 U30 ( .A(in[5]), .ZN(n67) );
  INV_X4 U31 ( .A(in[4]), .ZN(n68) );
  INV_X4 U32 ( .A(in[3]), .ZN(n69) );
  INV_X4 U33 ( .A(in[2]), .ZN(n70) );
  INV_X4 U34 ( .A(in[1]), .ZN(n71) );
  AND2_X4 U36 ( .A1(n83), .A2(n84), .ZN(zero) );
  AOI22_X2 U41 ( .A1(U4_DATA3_2), .A2(n63), .B1(U4_DATA2_2), .B2(n73), .ZN(n77) );
  OAI21_X2 U45 ( .B1(n54), .B2(n56), .A(n79), .ZN(n91) );
  DFF_X2 count_value_reg_0_ ( .D(n92), .CK(clock), .Q(sub_26_A_0_) );
  DFF_X2 count_value_reg_7_ ( .D(n91), .CK(clock), .Q(U4_DATA3_6), .QN(n54) );
  XNOR2_X1 sub_26_U1_A_1 ( .A(U4_DATA3_0), .B(sub_26_A_0_), .ZN(U4_DATA2_1) );
  OR2_X2 sub_26_U1_B_1 ( .A1(U4_DATA3_0), .A2(sub_26_A_0_), .ZN(
        sub_26_carry_2_) );
  OR2_X2 sub_26_U1_B_6 ( .A1(U4_DATA3_5), .A2(sub_26_carry_6_), .ZN(
        sub_26_carry_7_) );
  DFF_X2 count_value_reg_2_ ( .D(n89), .CK(clock), .Q(U4_DATA3_1), .QN(n61) );
  AOI22_X4 U37 ( .A1(U4_DATA2_7), .A2(n73), .B1(in[7]), .B2(latch), .ZN(n79)
         );
  NAND2_X2 U38 ( .A1(n61), .A2(n93), .ZN(sub_26_carry_5_) );
  AND2_X4 U39 ( .A1(n94), .A2(n59), .ZN(n93) );
  AOI22_X2 U40 ( .A1(U4_DATA3_6), .A2(n63), .B1(U4_DATA2_6), .B2(n73), .ZN(n72) );
  AOI22_X1 U42 ( .A1(U4_DATA3_1), .A2(n63), .B1(U4_DATA2_1), .B2(n73), .ZN(n78) );
  AOI22_X1 U43 ( .A1(U4_DATA3_3), .A2(n63), .B1(U4_DATA2_3), .B2(n73), .ZN(n76) );
  AOI22_X1 U44 ( .A1(U4_DATA3_4), .A2(n63), .B1(U4_DATA2_4), .B2(n73), .ZN(n75) );
  AOI22_X1 U46 ( .A1(U4_DATA3_5), .A2(n63), .B1(U4_DATA2_5), .B2(n73), .ZN(n74) );
  NOR3_X2 U47 ( .A1(zero), .A2(latch), .A3(n65), .ZN(n73) );
  NAND2_X1 U48 ( .A1(n61), .A2(n94), .ZN(sub_26_carry_4_) );
  NOR2_X1 U49 ( .A1(sub_26_carry_2_), .A2(U4_DATA3_2), .ZN(n94) );
  OAI221_X1 U50 ( .B1(n66), .B2(n64), .C1(n56), .C2(n57), .A(n72), .ZN(n85) );
  OAI221_X1 U51 ( .B1(n81), .B2(n62), .C1(sub_26_A_0_), .C2(n55), .A(n82), 
        .ZN(n92) );
  AOI22_X1 U52 ( .A1(in[0]), .A2(latch), .B1(sub_26_A_0_), .B2(n80), .ZN(n82)
         );
  NOR4_X1 U53 ( .A1(sub_26_A_0_), .A2(U4_DATA3_6), .A3(U4_DATA3_5), .A4(
        U4_DATA3_4), .ZN(n84) );
endmodule

