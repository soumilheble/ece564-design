	

	read_verilog -rtl /afs/unity.ncsu.edu/users/s/sheble/Projects/ece564/ece564_design/hw_4/q5/PR/run_f/counter_routed.v 
	current_design counter
	set clkname clock
	set CLK_PER 10
 	create_clock -name clock -period 10 -waveform "0 [expr 10 / 2]" clock
        source ../run_s/.synopsys_dc.setup
        source ./designenv.tcl	
	#report_power -hier -hier_level 3 -analysis_effort medium > counter_pwr_base.rpt
	report_power -analysis_effort medium > counter_pwr_base.rpt
	
	read_saif -input /afs/unity.ncsu.edu/users/s/sheble/Projects/ece564/ece564_design/hw_4/q5/SIMULATION/run_f/count.saif -instance_name test_fixture/u1

	#report_power -hier -hier_level 3  -analysis_effort medium > counter_pwr_pre_annotation.rpt
	report_power -analysis_effort medium > counter_pwr_pre_annotation.rpt
	report_clock_tree_power > counter_clock_pwr_pre_annotation.rpt
	report_timing > counter_timing_pre_annotation.rpt

	read_parasitics /afs/unity.ncsu.edu/users/s/sheble/Projects/ece564/ece564_design/hw_4/q5/PR/run_f/counter.spef -net_cap_only -complete_with wlm
 
	#report_power -hier -hier_level 3 -analysis_effort medium  > counter_pwr_post_annotation.rpt
	report_power -analysis_effort medium  > counter_pwr_post_annotation.rpt
	report_clock_tree_power > counter_clock_pwr_post_annotation.rpt
	report_timing > counter_timing_post_annotation.rpt
	exit
