/*
 * Name: RequestQueue.v
 * Date/Time: 10/13/2018 2:33
 * Author: Soumil Krishnanand Heble
 * ECE564 HW5 Arbiter Verilog Code - One Hot Encoding
 */

module RequestQueue (input clock, input reset, input wire R0, input wire R1, output reg G0, output reg G1);

parameter [4:0] // synopsys enum states
	S0 = 5'b00001,
	S1 = 5'b00010,
	S2 = 5'b00100,
	S3 = 5'b01000,
	S4 = 5'b10000;

reg [4:0] /* synopsys enum states */ current_state, next_state;
// synopsys state_vector current_state

/*------- Sequential Logic ----*/
always@(posedge clock, negedge reset)
begin
	if(!reset)
	begin
		current_state <= S0;
	end
	else
	begin
		current_state <= next_state;
	end
end

/* next state logic and output logic - combined so as to share state decode
 * logic */
always@(*)
begin
	/* defaults to prevent latches */
	G0 = 0;
	G1 = 0;
	/* FSM implementation using case */
	case(current_state) // synopsys full_case parallel_case
		S0: begin
			G0 = 0; 
			G1 = 0;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S3;
			end
			else
			begin
				next_state = S4;
			end
		    end
		S1: begin
			G0 = 1; 
			G1 = 0;
			next_state = S2;
		    end
		S2: begin
			G0 = 0; 
			G1 = 1;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S3;
			end
			else
			begin
				next_state = S4;
			end
		    end
		S3: begin
			G0 = 1;
			G1 = 0;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S3;
			end
			else
			begin
				next_state = S4;
			end
		    end
		S4: begin
			G0 = 0;
			G1 = 1;
			if({R0,R1}==2'b00)
			begin
				next_state = S0;
			end
			else if({R0,R1}==2'b11)
			begin
				next_state = S1;
			end
			else if({R0,R1}==2'b10)
			begin
				next_state = S3;
			end
			else
			begin
				next_state = S4;
			end
		    end
		default: next_state = S0;
	endcase
end
endmodule
