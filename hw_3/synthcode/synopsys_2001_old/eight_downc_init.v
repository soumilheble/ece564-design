
module eight_downc ( clock, latch, dec, div_by_two, in, zero );
  input [7:0] in;
  input clock, latch, dec, div_by_two;
  output zero;
  wire   \U4/DATA3_0 , \U4/DATA3_1 , \U4/DATA3_2 , \U4/DATA3_3 , \U4/DATA3_4 ,
         \U4/DATA3_5 , \U4/DATA3_6 , \U4/DATA2_1 , \U4/DATA2_2 , \U4/DATA2_3 ,
         \U4/DATA2_4 , \U4/DATA2_5 , \U4/DATA2_6 , \U4/DATA2_7 , \U4/DATA1_0 ,
         \U4/DATA1_1 , \U4/DATA1_2 , \U4/DATA1_3 , \U4/DATA1_4 , \U4/DATA1_5 ,
         \U4/DATA1_6 , \U4/DATA1_7 , \sub_22/carry[2] , \sub_22/carry[3] ,
         \sub_22/carry[4] , \sub_22/carry[5] , \sub_22/carry[6] ,
         \sub_22/carry[7] , \sub_22/A[0] , n56, n57, n58, n59, n60, n61, n62,
         n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76,
         n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
         n91, n92, n93, n94;
  assign \U4/DATA1_0  = in[0];
  assign \U4/DATA1_1  = in[1];
  assign \U4/DATA1_2  = in[2];
  assign \U4/DATA1_3  = in[3];
  assign \U4/DATA1_4  = in[4];
  assign \U4/DATA1_5  = in[5];
  assign \U4/DATA1_6  = in[6];
  assign \U4/DATA1_7  = in[7];

  XNOR2_X2 \sub_22/U1_A_1  ( .A(\U4/DATA3_0 ), .B(\sub_22/A[0] ), .ZN(
        \U4/DATA2_1 ) );
  OR2_X1 \sub_22/U1_B_1  ( .A1(\U4/DATA3_0 ), .A2(\sub_22/A[0] ), .ZN(
        \sub_22/carry[2] ) );
  XNOR2_X2 \sub_22/U1_A_2  ( .A(\U4/DATA3_1 ), .B(\sub_22/carry[2] ), .ZN(
        \U4/DATA2_2 ) );
  OR2_X1 \sub_22/U1_B_2  ( .A1(\U4/DATA3_1 ), .A2(\sub_22/carry[2] ), .ZN(
        \sub_22/carry[3] ) );
  XNOR2_X2 \sub_22/U1_A_3  ( .A(\U4/DATA3_2 ), .B(\sub_22/carry[3] ), .ZN(
        \U4/DATA2_3 ) );
  OR2_X1 \sub_22/U1_B_3  ( .A1(\U4/DATA3_2 ), .A2(\sub_22/carry[3] ), .ZN(
        \sub_22/carry[4] ) );
  XNOR2_X2 \sub_22/U1_A_4  ( .A(\U4/DATA3_3 ), .B(\sub_22/carry[4] ), .ZN(
        \U4/DATA2_4 ) );
  OR2_X1 \sub_22/U1_B_4  ( .A1(\U4/DATA3_3 ), .A2(\sub_22/carry[4] ), .ZN(
        \sub_22/carry[5] ) );
  XNOR2_X2 \sub_22/U1_A_5  ( .A(\U4/DATA3_4 ), .B(\sub_22/carry[5] ), .ZN(
        \U4/DATA2_5 ) );
  OR2_X1 \sub_22/U1_B_5  ( .A1(\U4/DATA3_4 ), .A2(\sub_22/carry[5] ), .ZN(
        \sub_22/carry[6] ) );
  XNOR2_X2 \sub_22/U1_A_6  ( .A(\U4/DATA3_5 ), .B(\sub_22/carry[6] ), .ZN(
        \U4/DATA2_6 ) );
  OR2_X1 \sub_22/U1_B_6  ( .A1(\U4/DATA3_5 ), .A2(\sub_22/carry[6] ), .ZN(
        \sub_22/carry[7] ) );
  XNOR2_X2 \sub_22/U1_A_7  ( .A(\U4/DATA3_6 ), .B(\sub_22/carry[7] ), .ZN(
        \U4/DATA2_7 ) );
  DFF_X1 \count_value_reg[0]  ( .D(n94), .CK(clock), .Q(\sub_22/A[0] ) );
  DFF_X1 \count_value_reg[7]  ( .D(n93), .CK(clock), .Q(\U4/DATA3_6 ), .QN(n56) );
  DFF_X1 \count_value_reg[6]  ( .D(n87), .CK(clock), .Q(\U4/DATA3_5 ), .QN(n59) );
  DFF_X1 \count_value_reg[5]  ( .D(n88), .CK(clock), .Q(\U4/DATA3_4 ), .QN(n60) );
  DFF_X1 \count_value_reg[4]  ( .D(n89), .CK(clock), .Q(\U4/DATA3_3 ), .QN(n61) );
  DFF_X1 \count_value_reg[3]  ( .D(n90), .CK(clock), .Q(\U4/DATA3_2 ), .QN(n62) );
  DFF_X1 \count_value_reg[2]  ( .D(n91), .CK(clock), .Q(\U4/DATA3_1 ), .QN(n63) );
  DFF_X1 \count_value_reg[1]  ( .D(n92), .CK(clock), .Q(\U4/DATA3_0 ), .QN(n64) );
  OAI221_X2 U3 ( .B1(n68), .B2(n66), .C1(n58), .C2(n59), .A(n74), .ZN(n87) );
  OAI221_X2 U5 ( .B1(n66), .B2(n69), .C1(n58), .C2(n60), .A(n76), .ZN(n88) );
  OAI221_X2 U7 ( .B1(n66), .B2(n70), .C1(n58), .C2(n61), .A(n77), .ZN(n89) );
  OAI221_X2 U9 ( .B1(n66), .B2(n71), .C1(n58), .C2(n62), .A(n78), .ZN(n90) );
  OAI221_X2 U11 ( .B1(n66), .B2(n72), .C1(n58), .C2(n63), .A(n79), .ZN(n91) );
  OAI221_X2 U13 ( .B1(n66), .B2(n73), .C1(n58), .C2(n64), .A(n80), .ZN(n92) );
  OAI221_X2 U17 ( .B1(n83), .B2(n64), .C1(\sub_22/A[0] ), .C2(n57), .A(n84), 
        .ZN(n94) );
  NOR3_X2 U19 ( .A1(n75), .A2(latch), .A3(n65), .ZN(n82) );
  NOR4_X2 U21 ( .A1(\sub_22/A[0] ), .A2(\U4/DATA3_6 ), .A3(\U4/DATA3_5 ), .A4(
        \U4/DATA3_4 ), .ZN(n86) );
  NOR4_X2 U22 ( .A1(\U4/DATA3_3 ), .A2(\U4/DATA3_2 ), .A3(\U4/DATA3_1 ), .A4(
        \U4/DATA3_0 ), .ZN(n85) );
  NAND3_X2 U23 ( .A1(n67), .A2(n66), .A3(div_by_two), .ZN(n83) );
  INV_X4 U24 ( .A(n75), .ZN(n57) );
  INV_X4 U25 ( .A(n82), .ZN(n58) );
  INV_X4 U26 ( .A(n83), .ZN(n65) );
  INV_X4 U27 ( .A(latch), .ZN(n66) );
  INV_X4 U28 ( .A(dec), .ZN(n67) );
  INV_X4 U29 ( .A(\U4/DATA1_6 ), .ZN(n68) );
  INV_X4 U30 ( .A(\U4/DATA1_5 ), .ZN(n69) );
  INV_X4 U31 ( .A(\U4/DATA1_4 ), .ZN(n70) );
  INV_X4 U32 ( .A(\U4/DATA1_3 ), .ZN(n71) );
  INV_X4 U33 ( .A(\U4/DATA1_2 ), .ZN(n72) );
  INV_X4 U34 ( .A(\U4/DATA1_1 ), .ZN(n73) );
  AND2_X4 U36 ( .A1(n85), .A2(n86), .ZN(zero) );
  NOR3_X1 U37 ( .A1(zero), .A2(latch), .A3(n67), .ZN(n75) );
  AOI22_X2 U38 ( .A1(\U4/DATA1_0 ), .A2(latch), .B1(\sub_22/A[0] ), .B2(n82), 
        .ZN(n84) );
  AOI22_X2 U39 ( .A1(\U4/DATA3_6 ), .A2(n65), .B1(\U4/DATA2_6 ), .B2(n75), 
        .ZN(n74) );
  AOI22_X2 U40 ( .A1(\U4/DATA3_1 ), .A2(n65), .B1(\U4/DATA2_1 ), .B2(n75), 
        .ZN(n80) );
  AOI22_X2 U41 ( .A1(\U4/DATA3_2 ), .A2(n65), .B1(\U4/DATA2_2 ), .B2(n75), 
        .ZN(n79) );
  AOI22_X2 U42 ( .A1(\U4/DATA3_3 ), .A2(n65), .B1(\U4/DATA2_3 ), .B2(n75), 
        .ZN(n78) );
  AOI22_X2 U43 ( .A1(\U4/DATA3_4 ), .A2(n65), .B1(\U4/DATA2_4 ), .B2(n75), 
        .ZN(n77) );
  AOI22_X2 U44 ( .A1(\U4/DATA3_5 ), .A2(n65), .B1(\U4/DATA2_5 ), .B2(n75), 
        .ZN(n76) );
  OAI21_X2 U45 ( .B1(n56), .B2(n58), .A(n81), .ZN(n93) );
  AOI22_X2 U46 ( .A1(\U4/DATA2_7 ), .A2(n75), .B1(\U4/DATA1_7 ), .B2(latch), 
        .ZN(n81) );
endmodule

