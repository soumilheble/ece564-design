
module RequestQueue ( clock, reset, R0, R1, G0, G1 );
  input clock, reset, R0, R1;
  output G0, G1;
  wire   U5_Z_0, U4_Z_0, U4_Z_1, U4_Z_2, n66, n67, n68, n69, n70, n71, n72,
         n73, n74, n75, n76, n77, n78, n79;
  wire   [49:45] n;

  DFFS_X2 current_state_reg_0_ ( .D(U5_Z_0), .CK(clock), .SN(n79), .Q(n[45])
         );
  DFFR_X1 current_state_reg_4_ ( .D(U4_Z_2), .CK(clock), .RN(n79), .Q(n[49])
         );
  DFFR_X1 current_state_reg_3_ ( .D(U4_Z_1), .CK(clock), .RN(n79), .Q(n[48])
         );
  DFFR_X1 current_state_reg_1_ ( .D(U4_Z_0), .CK(clock), .RN(n79), .Q(n[46]), 
        .QN(n67) );
  SDFFR_X2 current_state_reg_2_ ( .D(n71), .SI(1'b0), .SE(n67), .CK(clock), 
        .RN(n79), .Q(n[47]) );
  OAI221_X2 U4 ( .B1(n[46]), .B2(n70), .C1(n71), .C2(n67), .A(n72), .ZN(U5_Z_0) );
  NOR3_X2 U6 ( .A1(n69), .A2(R0), .A3(n73), .ZN(U4_Z_2) );
  NOR3_X2 U7 ( .A1(n68), .A2(R1), .A3(n73), .ZN(U4_Z_1) );
  NOR3_X2 U8 ( .A1(n68), .A2(n73), .A3(n69), .ZN(U4_Z_0) );
  NAND3_X2 U9 ( .A1(n66), .A2(n67), .A3(n72), .ZN(n73) );
  NOR2_X2 U11 ( .A1(n75), .A2(n[49]), .ZN(n71) );
  NOR4_X2 U12 ( .A1(n[48]), .A2(n[46]), .A3(n[45]), .A4(n76), .ZN(G1) );
  XNOR2_X2 U13 ( .A(n[47]), .B(n[49]), .ZN(n76) );
  NOR4_X2 U14 ( .A1(n[49]), .A2(n[47]), .A3(n[45]), .A4(n77), .ZN(G0) );
  XNOR2_X2 U15 ( .A(n[48]), .B(n[46]), .ZN(n77) );
  INV_X4 U16 ( .A(n71), .ZN(n66) );
  INV_X4 U17 ( .A(R0), .ZN(n68) );
  INV_X4 U18 ( .A(R1), .ZN(n69) );
  OR2_X1 U19 ( .A1(n74), .A2(n[48]), .ZN(n75) );
  OR2_X1 U20 ( .A1(n[45]), .A2(n[47]), .ZN(n74) );
  INV_X1 U21 ( .A(reset), .ZN(n78) );
  INV_X4 U22 ( .A(n78), .ZN(n79) );
  AOI222_X1 U23 ( .A1(n[45]), .A2(n[47]), .B1(n74), .B2(n[48]), .C1(n75), .C2(
        n[49]), .ZN(n72) );
  AOI21_X2 U24 ( .B1(n68), .B2(n69), .A(n71), .ZN(n70) );
endmodule

