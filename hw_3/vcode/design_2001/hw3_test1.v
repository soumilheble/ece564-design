/*
 * Name: hw3_test1.v
 * Date Created: 09/16/2018 18:12
 * Author: Soumil Krishnanand Heble
 * Module Name: hw3_test1
 * Test Script for Eight Bit Down Counter
 */ 

module hw3_test1;
	
	/* Variable Declaration */
	reg clock100 = 0;
	reg latch = 0;
	reg dec = 0;
	reg div_by_two = 0;
	reg [7:0] in = 8'b00010000;	// Load 0x10 (16) into in reg
	wire zero;
	
	initial
		begin
			$dumpfile("hw3_test1.vcd");	// Dump waveforms to this file
			$dumpvars;	// Dump all waveforms
			#16 latch = 1;	// Wait 16ns set latch to bring in data from in to internal count_value reg 
			#10 latch = 0;	// Wait 10ns clear latch 
			#10 dec = 1;	// Wait 10ns set dec to decrement count_value reg 
			#40 dec = 0;	// Wait 40ns clear dec count_value reg drops to 0x0C (12) 
			#10 div_by_two = 1;	// Wait 10ns then set div_by_two to begin dividing count_value by two 
			#20 div_by_two = 0;	// Wait 20ns clear div_by_two count_value drops to 0x03 (3) 
			#10 dec = 1;	// Wait 10ns set dec to begin decrementing count_value 
			#40 $finish;	// Wait 40ns count_value drops to 0x00 (0) and zero is asserted
		end
	always #5 clock100 = ~clock100;	// Toggle clock every 5ns (Period 10ns - 50% Duty)
	
	/* Instantiate Down Counter as edc1 */
	eight_downc edc1 (.clock(clock100), .latch(latch), .dec(dec), .div_by_two(div_by_two), .in(in), .zero(zero));
endmodule
