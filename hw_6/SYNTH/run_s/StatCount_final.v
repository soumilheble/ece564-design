
module StatCount ( clock, reset, clear, DataIn1, DataIn2, EvenParity, GreyCode, 
        overflow );
  input [7:0] DataIn1;
  input [7:0] DataIn2;
  output [7:0] EvenParity;
  output [7:0] GreyCode;
  input clock, reset, clear;
  output overflow;
  wire   U10_Z_0, U7_Z_0, U7_Z_1, U7_Z_2, U7_Z_3, U7_Z_4, U7_Z_5, U7_Z_6,
         U7_Z_7, U7_DATA1_0, U7_DATA1_1, U7_DATA1_2, U7_DATA1_3, U7_DATA1_4,
         U7_DATA1_5, U7_DATA1_6, U7_DATA1_7, U5_Z_0, U5_Z_1, U5_Z_2, U5_Z_3,
         U5_Z_4, U5_Z_5, U5_Z_6, U5_Z_7, U5_DATA1_0, U5_DATA1_1, U5_DATA1_2,
         U5_DATA1_3, U5_DATA1_4, U5_DATA1_5, U5_DATA1_6, U5_DATA1_7,
         add_108_B_0_, add_108_B_1_, add_61_B_0_, add_61_B_1_, n127, n128,
         n129, n130, n131, n132, n133, n134, n135, n136, n137, n138, n139,
         n140, n141, n142, n143, n144, n145, n146, n147, n148, n149, n150,
         n151, n152, n153, n154, n155, n156, n157, n158, n159, n160, n161,
         n162, n163, n164;
  wire   [7:2] add_108_carry;
  wire   [7:2] add_61_carry;

  FA_X1 add_61_U1_1 ( .A(EvenParity[1]), .B(add_61_B_1_), .CI(n147), .CO(
        add_61_carry[2]), .S(U7_DATA1_1) );
  FA_X1 add_108_U1_1 ( .A(GreyCode[1]), .B(add_108_B_1_), .CI(n146), .CO(
        add_108_carry[2]), .S(U5_DATA1_1) );
  NOR2_X2 U3 ( .A1(n128), .A2(n129), .ZN(add_61_B_1_) );
  XOR2_X2 U4 ( .A(n128), .B(n129), .Z(add_61_B_0_) );
  XNOR2_X2 U5 ( .A(n130), .B(n131), .ZN(n129) );
  XOR2_X2 U6 ( .A(n132), .B(n133), .Z(n131) );
  XNOR2_X2 U7 ( .A(n134), .B(n135), .ZN(n130) );
  XNOR2_X2 U10 ( .A(n136), .B(n137), .ZN(n128) );
  XOR2_X2 U11 ( .A(n138), .B(n139), .Z(n137) );
  XNOR2_X2 U12 ( .A(n140), .B(n141), .ZN(n136) );
  NOR2_X2 U13 ( .A1(n142), .A2(n143), .ZN(add_108_B_1_) );
  XOR2_X2 U14 ( .A(n143), .B(n142), .Z(add_108_B_0_) );
  NAND4_X2 U15 ( .A1(n140), .A2(n141), .A3(n139), .A4(n138), .ZN(n142) );
  XOR2_X2 U16 ( .A(DataIn2[7]), .B(DataIn2[6]), .Z(n138) );
  XOR2_X2 U17 ( .A(DataIn2[5]), .B(DataIn2[4]), .Z(n139) );
  XOR2_X2 U18 ( .A(DataIn2[1]), .B(DataIn2[0]), .Z(n141) );
  XOR2_X2 U19 ( .A(DataIn2[3]), .B(DataIn2[2]), .Z(n140) );
  NAND4_X2 U20 ( .A1(n134), .A2(n135), .A3(n133), .A4(n132), .ZN(n143) );
  XOR2_X2 U21 ( .A(DataIn1[7]), .B(DataIn1[6]), .Z(n132) );
  XOR2_X2 U22 ( .A(DataIn1[5]), .B(DataIn1[4]), .Z(n133) );
  XOR2_X2 U23 ( .A(DataIn1[1]), .B(DataIn1[0]), .Z(n135) );
  XOR2_X2 U24 ( .A(DataIn1[3]), .B(DataIn1[2]), .Z(n134) );
  NOR2_X2 U25 ( .A1(clear), .A2(n144), .ZN(U10_Z_0) );
  NOR3_X2 U26 ( .A1(n145), .A2(overflow), .A3(n148), .ZN(n144) );
  INV_X4 U27 ( .A(clear), .ZN(n127) );
  AND2_X1 U28 ( .A1(U7_DATA1_7), .A2(n127), .ZN(U7_Z_7) );
  AND2_X1 U29 ( .A1(U7_DATA1_6), .A2(n127), .ZN(U7_Z_6) );
  AND2_X1 U30 ( .A1(U7_DATA1_5), .A2(n127), .ZN(U7_Z_5) );
  AND2_X1 U31 ( .A1(U7_DATA1_4), .A2(n127), .ZN(U7_Z_4) );
  AND2_X1 U32 ( .A1(U7_DATA1_3), .A2(n127), .ZN(U7_Z_3) );
  AND2_X1 U33 ( .A1(U7_DATA1_2), .A2(n127), .ZN(U7_Z_2) );
  AND2_X1 U34 ( .A1(U7_DATA1_1), .A2(n127), .ZN(U7_Z_1) );
  AND2_X1 U35 ( .A1(U7_DATA1_0), .A2(n127), .ZN(U7_Z_0) );
  AND2_X1 U36 ( .A1(U5_DATA1_7), .A2(n127), .ZN(U5_Z_7) );
  AND2_X1 U37 ( .A1(U5_DATA1_6), .A2(n127), .ZN(U5_Z_6) );
  AND2_X1 U38 ( .A1(U5_DATA1_5), .A2(n127), .ZN(U5_Z_5) );
  AND2_X1 U39 ( .A1(U5_DATA1_4), .A2(n127), .ZN(U5_Z_4) );
  AND2_X1 U40 ( .A1(U5_DATA1_3), .A2(n127), .ZN(U5_Z_3) );
  AND2_X1 U41 ( .A1(U5_DATA1_2), .A2(n127), .ZN(U5_Z_2) );
  AND2_X1 U42 ( .A1(U5_DATA1_1), .A2(n127), .ZN(U5_Z_1) );
  AND2_X1 U43 ( .A1(U5_DATA1_0), .A2(n127), .ZN(U5_Z_0) );
  DFFR_X2 GreyCode_reg_2_ ( .D(U5_Z_2), .CK(clock), .RN(n150), .Q(GreyCode[2])
         );
  DFFR_X2 GreyCode_reg_3_ ( .D(U5_Z_3), .CK(clock), .RN(n150), .Q(GreyCode[3])
         );
  DFFR_X2 GreyCode_reg_4_ ( .D(U5_Z_4), .CK(clock), .RN(n150), .Q(GreyCode[4])
         );
  DFFR_X2 EvenParity_reg_2_ ( .D(U7_Z_2), .CK(clock), .RN(n150), .Q(
        EvenParity[2]) );
  DFFR_X2 GreyCode_reg_5_ ( .D(U5_Z_5), .CK(clock), .RN(n150), .Q(GreyCode[5])
         );
  DFFR_X2 EvenParity_reg_3_ ( .D(U7_Z_3), .CK(clock), .RN(n150), .Q(
        EvenParity[3]) );
  DFFR_X2 GreyCode_reg_6_ ( .D(U5_Z_6), .CK(clock), .RN(n150), .Q(GreyCode[6])
         );
  DFFR_X2 EvenParity_reg_4_ ( .D(U7_Z_4), .CK(clock), .RN(n150), .Q(
        EvenParity[4]) );
  DFFR_X2 GreyCode_reg_7_ ( .D(U5_Z_7), .CK(clock), .RN(n150), .Q(GreyCode[7])
         );
  DFFR_X2 EvenParity_reg_5_ ( .D(U7_Z_5), .CK(clock), .RN(n150), .Q(
        EvenParity[5]) );
  DFFR_X2 EvenParity_reg_6_ ( .D(U7_Z_6), .CK(clock), .RN(n150), .Q(
        EvenParity[6]) );
  DFFR_X2 EvenParity_reg_7_ ( .D(U7_Z_7), .CK(clock), .RN(n150), .Q(
        EvenParity[7]) );
  DFFR_X2 GreyCode_reg_1_ ( .D(U5_Z_1), .CK(clock), .RN(n150), .Q(GreyCode[1])
         );
  DFFR_X2 EvenParity_reg_1_ ( .D(U7_Z_1), .CK(clock), .RN(n150), .Q(
        EvenParity[1]) );
  DFFR_X2 overflow_reg ( .D(U10_Z_0), .CK(clock), .RN(n150), .Q(overflow) );
  DFFR_X2 GreyCode_reg_0_ ( .D(U5_Z_0), .CK(clock), .RN(n150), .Q(GreyCode[0]), 
        .QN(n151) );
  DFFR_X2 EvenParity_reg_0_ ( .D(U7_Z_0), .CK(clock), .RN(n150), .Q(
        EvenParity[0]), .QN(n158) );
  AND2_X4 U44 ( .A1(GreyCode[7]), .A2(add_108_carry[7]), .ZN(n145) );
  AND2_X4 U45 ( .A1(add_108_B_0_), .A2(GreyCode[0]), .ZN(n146) );
  AND2_X4 U46 ( .A1(add_61_B_0_), .A2(EvenParity[0]), .ZN(n147) );
  AND2_X4 U47 ( .A1(EvenParity[7]), .A2(add_61_carry[7]), .ZN(n148) );
  INV_X1 U48 ( .A(reset), .ZN(n149) );
  INV_X8 U49 ( .A(n149), .ZN(n150) );
  XNOR2_X2 U50 ( .A(add_108_B_0_), .B(n151), .ZN(U5_DATA1_0) );
  INV_X4 U51 ( .A(add_108_carry[2]), .ZN(n152) );
  XNOR2_X2 U52 ( .A(GreyCode[2]), .B(n152), .ZN(U5_DATA1_2) );
  NAND2_X2 U53 ( .A1(GreyCode[2]), .A2(add_108_carry[2]), .ZN(n153) );
  INV_X4 U54 ( .A(n153), .ZN(add_108_carry[3]) );
  XNOR2_X2 U55 ( .A(GreyCode[3]), .B(n153), .ZN(U5_DATA1_3) );
  NAND2_X2 U56 ( .A1(GreyCode[3]), .A2(add_108_carry[3]), .ZN(n154) );
  INV_X4 U57 ( .A(n154), .ZN(add_108_carry[4]) );
  XNOR2_X2 U58 ( .A(GreyCode[4]), .B(n154), .ZN(U5_DATA1_4) );
  NAND2_X2 U59 ( .A1(GreyCode[4]), .A2(add_108_carry[4]), .ZN(n155) );
  INV_X4 U60 ( .A(n155), .ZN(add_108_carry[5]) );
  XNOR2_X2 U61 ( .A(GreyCode[5]), .B(n155), .ZN(U5_DATA1_5) );
  NAND2_X2 U62 ( .A1(GreyCode[5]), .A2(add_108_carry[5]), .ZN(n156) );
  INV_X4 U63 ( .A(n156), .ZN(add_108_carry[6]) );
  XNOR2_X2 U64 ( .A(GreyCode[6]), .B(n156), .ZN(U5_DATA1_6) );
  NAND2_X2 U65 ( .A1(GreyCode[6]), .A2(add_108_carry[6]), .ZN(n157) );
  INV_X4 U66 ( .A(n157), .ZN(add_108_carry[7]) );
  XNOR2_X2 U67 ( .A(GreyCode[7]), .B(n157), .ZN(U5_DATA1_7) );
  XNOR2_X2 U68 ( .A(add_61_B_0_), .B(n158), .ZN(U7_DATA1_0) );
  INV_X4 U69 ( .A(add_61_carry[2]), .ZN(n159) );
  XNOR2_X2 U70 ( .A(EvenParity[2]), .B(n159), .ZN(U7_DATA1_2) );
  NAND2_X2 U71 ( .A1(EvenParity[2]), .A2(add_61_carry[2]), .ZN(n160) );
  INV_X4 U72 ( .A(n160), .ZN(add_61_carry[3]) );
  XNOR2_X2 U73 ( .A(EvenParity[3]), .B(n160), .ZN(U7_DATA1_3) );
  NAND2_X2 U74 ( .A1(EvenParity[3]), .A2(add_61_carry[3]), .ZN(n161) );
  INV_X4 U75 ( .A(n161), .ZN(add_61_carry[4]) );
  XNOR2_X2 U76 ( .A(EvenParity[4]), .B(n161), .ZN(U7_DATA1_4) );
  NAND2_X2 U77 ( .A1(EvenParity[4]), .A2(add_61_carry[4]), .ZN(n162) );
  INV_X4 U78 ( .A(n162), .ZN(add_61_carry[5]) );
  XNOR2_X2 U79 ( .A(EvenParity[5]), .B(n162), .ZN(U7_DATA1_5) );
  NAND2_X2 U80 ( .A1(EvenParity[5]), .A2(add_61_carry[5]), .ZN(n163) );
  INV_X4 U81 ( .A(n163), .ZN(add_61_carry[6]) );
  XNOR2_X2 U82 ( .A(EvenParity[6]), .B(n163), .ZN(U7_DATA1_6) );
  NAND2_X2 U83 ( .A1(EvenParity[6]), .A2(add_61_carry[6]), .ZN(n164) );
  INV_X4 U84 ( .A(n164), .ZN(add_61_carry[7]) );
  XNOR2_X2 U85 ( .A(EvenParity[7]), .B(n164), .ZN(U7_DATA1_7) );
endmodule

