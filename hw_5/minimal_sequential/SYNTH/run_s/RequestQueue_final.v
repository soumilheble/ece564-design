
module RequestQueue ( clock, reset, R0, R1, G0, G1 );
  input clock, reset, R0, R1;
  output G0, G1;
  wire   U5_Z_0, U5_Z_1, U4_Z_0, n51, n52, n53, n54, n55, n56, n57, n58, n59,
         n60, n61;
  wire   [34:32] n;

  DFFR_X1 current_state_reg_0_ ( .D(U5_Z_0), .CK(clock), .RN(reset), .Q(n[32]), 
        .QN(n54) );
  DFFR_X1 current_state_reg_2_ ( .D(U5_Z_1), .CK(clock), .RN(reset), .Q(n[34]), 
        .QN(n52) );
  DFFR_X1 current_state_reg_1_ ( .D(U4_Z_0), .CK(clock), .RN(reset), .Q(n[33]), 
        .QN(n53) );
  NOR3_X2 U3 ( .A1(n55), .A2(R0), .A3(n51), .ZN(U5_Z_1) );
  NAND2_X2 U4 ( .A1(n57), .A2(n58), .ZN(U4_Z_0) );
  NAND3_X2 U5 ( .A1(n56), .A2(n55), .A3(R0), .ZN(n58) );
  NAND2_X2 U6 ( .A1(n59), .A2(n60), .ZN(n56) );
  NOR2_X2 U8 ( .A1(n[32]), .A2(n61), .ZN(G1) );
  XNOR2_X2 U9 ( .A(n[33]), .B(n[34]), .ZN(n61) );
  NAND2_X2 U10 ( .A1(n59), .A2(n57), .ZN(G0) );
  NAND3_X2 U11 ( .A1(n53), .A2(n52), .A3(n[32]), .ZN(n57) );
  NAND3_X2 U12 ( .A1(n[33]), .A2(n52), .A3(n[32]), .ZN(n59) );
  INV_X4 U13 ( .A(n56), .ZN(n51) );
  INV_X4 U14 ( .A(R1), .ZN(n55) );
  AND2_X1 U15 ( .A1(n56), .A2(R0), .ZN(U5_Z_0) );
  OAI21_X2 U16 ( .B1(n53), .B2(n52), .A(n54), .ZN(n60) );
endmodule

